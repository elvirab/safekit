//
//  SliderTableViewCell.swift
//  SafeKit
//
//  Created by Sergei Kazberovich on 6/19/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit

protocol SliderTableViewCellDelegate {
    func updatePoligonWithRadius(radius:Int)
}

class SliderTableViewCell: UITableViewCell {
    @IBOutlet weak var distanceAvaliable: UILabel?
    @IBOutlet weak var slider: UISlider?
    var delegate : SliderTableViewCellDelegate? = nil
    
    
    @IBAction func valueChanged(sender: UISlider) {
        let currentValue = Int(sender.value)
        distanceAvaliable!.text = "\(currentValue) m"
        delegate?.updatePoligonWithRadius(currentValue)
    }
}
