//
//  MonitoringViewController.swift
//  SafeKit
//
//  Created by Yury Khomchyk on 6/18/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit
import Mapbox
import KontaktSDK

enum DisplayingMode : Int {
    case Beacon = 0
    case Phone
}

class MonitoringViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MGLMapViewDelegate, SliderTableViewCellDelegate {

    // MARK: - Subviews
    
    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet var segmentedControl: UISegmentedControl!
    @IBOutlet var mapView: MGLMapView!
    
    var annotations : [MGLAnnotation] = []
    var monitoringMode = DisplayingMode.Beacon
    
    var polygon : MGLPolygon?
    var currentCoordinate : CLLocationCoordinate2D?
    // MARK: - Data
    
    var monitoringKid: Kid? {
        didSet {
            title = monitoringKid?.name.uppercaseString ?? ""
        }
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BeaconService.sharedInstance.startService(self)

        tableView?.delegate = self
        tableView?.dataSource = self
        mapView.delegate = self
        
        addCurrentLocationToMap()
        
        let doubleTap = UITapGestureRecognizer(target: self, action: nil)
        doubleTap.numberOfTapsRequired = 2
        mapView.addGestureRecognizer(doubleTap)

        let singleTap = UITapGestureRecognizer(target: self, action:#selector(handleSingleTap))
        singleTap.requireGestureRecognizerToFail(doubleTap)
        mapView.addGestureRecognizer(singleTap)

    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath
        ) -> UITableViewCell {
        
        if segmentedControl.selectedSegmentIndex == DisplayingMode.Beacon.rawValue && indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("SliderTableViewCell", forIndexPath: indexPath) as! SliderTableViewCell
            cell.delegate = self
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MonitoringTableViewCell", forIndexPath: indexPath) as! MonitoringTableViewCell
        if let name = monitoringKid?.name {
            cell.nameLabel?.text = name
            cell.distanceLabel?.text = monitoringKid!.age
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedControl.selectedSegmentIndex {
        case DisplayingMode.Beacon.rawValue:
            return 2
        case DisplayingMode.Phone.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    
    //MARK: - UISegmentedControl actions
    
    @IBAction func displayingModeChanged(sender: AnyObject) {
        setupDisplayMode(segmentedControl.selectedSegmentIndex)
    }
    
    //MARK: - Map delegate methods
    
    func mapView(mapView: MGLMapView, imageForAnnotation annotation: MGLAnnotation) -> MGLAnnotationImage? {
        return nil
    }
    
    func mapView(mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        return 0.5
    }
    func mapView(mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        return UIColor.whiteColor()
    }
    
    func mapView(mapView: MGLMapView, fillColorForPolygonAnnotation annotation: MGLPolygon) -> UIColor {
        return UIColor(red: 59/255, green: 178/255, blue: 208/255, alpha: 1)
    }
    
    //MARK: - Custom methods
    
    func setupDisplayMode(mode:Int) {
        clearMap()
        switch mode {
        case DisplayingMode.Beacon.rawValue:
//            addCurrentLocationToMap()
            monitoringMode = DisplayingMode.Beacon
            break

        case DisplayingMode.Phone.rawValue:
            addCurrentLocationToMap()
            monitoringMode = DisplayingMode.Phone
            addCurrentLocationToMap()
            break
            
        default:()
        }
        
        tableView?.reloadData()
    }
    
    func addCurrentLocationToMap() {
        mapView.setCenterCoordinate(CLLocationCoordinate2D(latitude: 53.907574, longitude: 27.563224), zoomLevel: 17, animated: true)
        view.addSubview(mapView)
        
        let currentLocation = MGLPointAnnotation()
        currentLocation.coordinate = CLLocationCoordinate2D(latitude: 53.907574, longitude: 27.563224)
        currentLocation.title = "Current Location"
        currentCoordinate = currentLocation.coordinate
        
        mapView.addAnnotation(currentLocation)
        annotations.append(currentLocation)
        
        polygonCircleForCoordinate(currentLocation.coordinate, withMeterRadius: 50)
    }
    
    func addKidToMap()
    {
    
    }

    
    func polygonCircleForCoordinate(coordinate: CLLocationCoordinate2D, withMeterRadius: Double) {
        let degreesBetweenPoints = 8.0
        //45 sides
        let numberOfPoints = floor(360.0 / degreesBetweenPoints)
        let distRadians: Double = withMeterRadius / 6371000.0
        // earth radius in meters
        let centerLatRadians: Double = coordinate.latitude * M_PI / 180
        let centerLonRadians: Double = coordinate.longitude * M_PI / 180
        var coordinates = [CLLocationCoordinate2D]()
        //array to hold all the points
        for var index = 0; index < Int(numberOfPoints); index += 1 {
            let degrees: Double = Double(index) * Double(degreesBetweenPoints)
            let degreeRadians: Double = degrees * M_PI / 180
            let pointLatRadians: Double = asin(sin(centerLatRadians) * cos(distRadians) + cos(centerLatRadians) * sin(distRadians) * cos(degreeRadians))
            let pointLonRadians: Double = centerLonRadians + atan2(sin(degreeRadians) * sin(distRadians) * cos(centerLatRadians), cos(distRadians) - sin(centerLatRadians) * sin(pointLatRadians))
            let pointLat: Double = pointLatRadians * 180 / M_PI
            let pointLon: Double = pointLonRadians * 180 / M_PI
            let point: CLLocationCoordinate2D = CLLocationCoordinate2DMake(pointLat, pointLon)
            coordinates.append(point)
        }
        
        self.polygon = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        self.mapView.addAnnotation(self.polygon!)
    }
    
    
    func clearMap() {
        mapView.removeAnnotations(annotations)
        if let polygon = self.polygon {
            mapView.removeAnnotation(polygon)
            self.polygon = nil
        }
    }
    
    //MARK: - UIGestureRecognizer
    
    func handleSingleTap(tap: UITapGestureRecognizer) {
        let location: CLLocationCoordinate2D = mapView.convertPoint(tap.locationInView(mapView), toCoordinateFromView: mapView)
        print("You tapped at: \(location.latitude), \(location.longitude)")
    }
    
    //Mark:- SliderTableViewCellDelegate

    func updatePoligonWithRadius(radius:Int){
        
        if let polygon = self.polygon{
            
            mapView.removeAnnotation(polygon)
            self.polygon = nil
            polygonCircleForCoordinate(currentCoordinate!, withMeterRadius: Double(radius))
        }
    }
}

extension MonitoringViewController: KTKBeaconManagerDelegate {
    func beaconManager(manager: KTKBeaconManager, didChangeLocationAuthorizationStatus status: CLAuthorizationStatus) {
    }
    
    func beaconManager(manager: KTKBeaconManager, didEnterRegion region: KTKBeaconRegion) {
        print("ENTER!")
        dispatch_async(dispatch_get_main_queue(), {
            let alert = UIAlertController(title: "Nice!", message:
                "Your child is in radius", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        })
    }
    
    func beaconManager(manager: KTKBeaconManager, didExitRegion region: KTKBeaconRegion) {
        print("EXIT!")
        dispatch_async(dispatch_get_main_queue(), {
            let alert = UIAlertController(title: "Warning!", message:
                "Your child is out of radius", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        })
    }
    
    func beaconManager(manager: KTKBeaconManager, didRangeBeacons beacons: [CLBeacon], inRegion region: KTKBeaconRegion) {
        if monitoringMode == DisplayingMode.Beacon {
            for beacon in beacons {
                if beacon.major == 4 {
                    print("proximity %@",beacon.proximity.rawValue)
                    NSLog("%f",beacon.accuracy)
                    
                }
            }
        }
        else if monitoringMode == DisplayingMode.Phone {
            
        }
    }
    
}
