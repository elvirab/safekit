//
//  MonitoringTableViewCell.swift
//  SafeKit
//
//  Created by  Elvira Burchik on 18/06/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit

class MonitoringTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var distanceLabel: UILabel?
    
}
