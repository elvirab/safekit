//
//  KidModel.swift
//  SafeKit
//
//  Created by  Elvira Burchik on 18/06/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import Foundation

struct Kid {
    
    let name: String
    let age: String
    
}
