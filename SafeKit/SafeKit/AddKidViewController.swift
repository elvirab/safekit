//
//  AddKidViewControllerScreen.swift
//  SafeKit
//
//  Created by  Elvira Burchik on 18/06/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit

private let startAge: NSInteger = 3
private let endAge: NSInteger = 18

protocol AddKidViewControllerDelegate: class {
    func didSaveData(sender: AddKidViewController, data: Kid)
}

class AddKidViewController: UIViewController {
    
    // MARK: - Subviews
    
    @IBOutlet private weak var pickerView: UIPickerView?
    @IBOutlet private weak var nameTextField: UITextField?
    
    weak var delegate:AddKidViewControllerDelegate?
    
    var pickerData: [String] {
        var range: [String] = []
        var age = startAge
        while age < endAge {
            range.append(String(age))
            age += 1
        }
        return range
    }
    
    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self, selector: #selector(
                AddKidViewController.keyboardWillShow),
            name: UIKeyboardWillShowNotification,
            object: nil
        )
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(AddKidViewController.keyboardWillHide),
            name: UIKeyboardWillHideNotification,
            object: nil
        )
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(
            self,
            name: UIKeyboardWillShowNotification,
            object: self.view.window
        )
        NSNotificationCenter.defaultCenter().removeObserver(
            self,
            name: UIKeyboardWillHideNotification,
            object: self.view.window
        )
    }
    
    @IBAction private func didNextButtonPressed(sender: UIButton) {
        if nameTextField?.text == "" {
            let alertController = UIAlertController(
                title: "Error",
                message:
                "You haven't entered name",
                preferredStyle: .Alert
            )
            alertController.addAction(
                UIAlertAction(
                    title: "Dismiss",
                    style: .Default,
                    handler: nil
                )
            )
            presentViewController(
                alertController,
                animated: true,
                completion: nil
            )
        } else {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let kidName = nameTextField?.text {
                if let age = pickerView?.selectedRowInComponent(0){
                    let kid = ["Name": kidName, "Age": String(age)]
                    userDefaults.setObject(kid, forKey: "kid")
                    
                    self.dismissViewControllerAnimated(true, completion: {
                        self.delegate?.didSaveData(self, data: Kid(name: kidName, age: String(age)))
                        }
                    )
                }
                
            }
        }
    }

}

private extension AddKidViewController {
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            view.frame.origin.y -= keyboardSize.height / 2
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            view.frame.origin.y += keyboardSize.height / 2
        }
    }
    
}

extension AddKidViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}

extension AddKidViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(
        pickerView: UIPickerView,
        numberOfRowsInComponent component: Int
    ) -> Int {
        return pickerData.count
    }
    
    func numberOfComponentsInPickerView(
        pickerView: UIPickerView
    ) -> Int {
        return 1
    }
    
}