//
//  KidModeViewController.swift
//  SafeKit
//
//  Created by Yury Khomchyk on 6/18/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit
import KontaktSDK

class KidModeViewController: UIViewController, KTKBeaconManagerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        BeaconService.sharedInstance.startService(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Beacon delegate methods
    
    func beaconManager(manager: KTKBeaconManager, didChangeLocationAuthorizationStatus status: CLAuthorizationStatus) {
    }
    
    func beaconManager(manager: KTKBeaconManager, didEnterRegion region: KTKBeaconRegion) {
        ParseManager.sharedInstance.incrementParentAge()
    }
    
    func beaconManager(manager: KTKBeaconManager, didExitRegion region: KTKBeaconRegion) {
        ParseManager.sharedInstance.incrementParentAge()
    }
    
    func beaconManager(manager: KTKBeaconManager, didRangeBeacons beacons: [CLBeacon], inRegion region: KTKBeaconRegion) {
        
    }

}
