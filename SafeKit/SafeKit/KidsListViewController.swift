//
//  KidsListViewController.swift
//  SafeKit
//
//  Created by  Elvira Burchik on 18/06/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit

class KidsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Subviews
    
    @IBOutlet private weak var tableView: UITableView?
    @IBOutlet private weak var addButton: UIButton?
    @IBOutlet private weak var noKidsView: UIView?
    @IBOutlet private weak var kidsListView: UIView?
    
    var currentKid: Kid?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.delegate = self
        tableView?.dataSource = self
        
        setupButton(addButton)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        showNeededInterface(nil)
        navigationController?.navigationBarHidden = true
    }
    
    private func setupButton(button: UIButton?) {
        button?.layer.borderWidth = 2.0
        button?.layer.borderColor = UIColor(
            red: 123 / 255.0,
            green: 53 / 255.0,
            blue: 60 / 255.0,
            alpha: 1.0
            ).CGColor
        button?.layer.cornerRadius = 10
    }
    
    // MARK: - Actions
    
    @IBAction private func addKidButtonPressed(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Startings", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("AddKidViewController") as? AddKidViewController {
            vc.delegate = self
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    private func showNeededInterface(forKid: Kid?) {
        if forKid != nil {
            currentKid = forKid
        }
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if forKid != nil || userDefaults.valueForKey("kid") != nil {
            noKidsView?.hidden = true
            kidsListView?.hidden = false
        } else {
            noKidsView?.hidden = false
            kidsListView?.hidden = true
        }
        tableView?.reloadData()
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(
        tableView: UITableView,
        cellForRowAtIndexPath
        indexPath: NSIndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(
            "KidsListTableViewCell",
            forIndexPath: indexPath
            ) as! KidsListTableViewCell
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let dict = userDefaults.objectForKey("kid") as? [String: String] ?? [String: String]()
        
        if let name = dict["Name"] {
            cell.nameLabel?.text = name
        } else if let name = currentKid?.name {
            cell.nameLabel?.text = name
        }
        
        return cell
    }
    
    func tableView(
        tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return 1
    }
    
    func tableView(
        tableView: UITableView,
        didSelectRowAtIndexPath
        indexPath: NSIndexPath
    ) {
        let storyboard = UIStoryboard(name: "Monitoring", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("MonitoringViewController") as? MonitoringViewController {
            navigationController?.navigationBar.barTintColor = UIColor(
                red: 123 / 255.0,
                green: 53 / 255.0,
                blue: 60 / 255.0,
                alpha: 1.0
            )
            let userDefaults = NSUserDefaults.standardUserDefaults()
            let dict = userDefaults.objectForKey("kid") as? [String: String] ?? [String: String]()
            vc.monitoringKid = Kid(name: dict["Name"]!, age: dict["Age"]!)
            
            navigationController?.navigationBar.tintColor = UIColor.whiteColor()
            navigationController?.navigationBarHidden = false
            let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
            navigationController?.pushViewController(vc, animated: true)
            
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

}

extension KidsListViewController: AddKidViewControllerDelegate {
    
    func didSaveData(sender: AddKidViewController, data: Kid) {
        showNeededInterface(data)
    }
}
