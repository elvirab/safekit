//
//  ParseManager.swift
//  SafeKit
//
//  Created by Sergei Kazberovich on 6/18/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit
import Parse

class ParseManager: NSObject {
    static let sharedInstance =  ParseManager()
    
    func login(username : String, password : String, callback: (success : Bool, error: NSError?) -> ()) {
        PFUser.logInWithUsernameInBackground("Melissa", password:"12345678") {
            (user: PFUser?, error: NSError?) -> () in
            
            if (error == nil) {
                callback(success: true, error: nil)
            } else
            {
                callback(success: false, error: error)
            }
        }
    }
    
    func fetchChildrenWithCallback(callback:(children : [User]?, error: NSError?) -> ()) {
        let query = PFQuery(className: "_User")

        query.whereKey("parent", equalTo: User.currentUser()!).findObjectsInBackgroundWithBlock({(let kids, let error) -> Void in
            
            if let kids = kids as? [User] where error == nil {
                print(kids)
                callback(children: kids, error: nil)
            } else {
                if let error = error {
                    callback(children: nil, error: error)
                }
            }
        })
    }
    
    func incrementParentAge() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId(User.currentUser()!.objectId!) {
            (user: PFObject?, error: NSError?) -> Void in
            if error != nil {
                print(error)
            } else if let user = user as? User {
                user["age"] = user.age + 1
                user.saveInBackgroundWithBlock({ (let isComplete, let error) in
                    print("")
                })
            }
        }
    }
}
