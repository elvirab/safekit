//
//  BeaconManager.swift
//  SafeKit
//
//  Created by Yury Khomchyk on 6/18/16.
//  Copyright © 2016 Elvira. All rights reserved.
//


import Foundation
import KontaktSDK

let beaconApiKey = "EWAPQhxJWpSHiFMNvyvRoQdLVUKUQIXU"

class BeaconService {
    class var sharedInstance : BeaconService {
        struct Singleton {
            static let instance = BeaconService()
            
        }
        return Singleton.instance
    }
    var beaconManager = KTKBeaconManager()
    var isAnyChildIsOutOfRange = false
    
    func startService(newDelegate:KTKBeaconManagerDelegate) {
        Kontakt.setAPIKey(beaconApiKey)
        beaconManager = KTKBeaconManager(delegate: newDelegate)
        beaconManager.requestLocationAlwaysAuthorization()
        let proximityUUID = NSUUID(UUIDString: "f7826da6-4fa2-4e98-8024-bc5b71e0893e")
        let region = KTKBeaconRegion(proximityUUID: proximityUUID!, identifier: "region")
        beaconManager.startMonitoringForRegion(region)
        beaconManager.startRangingBeaconsInRegion(region)
    }
    
    
    
    func sendNotification(message: String) {
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: 5)
        notification.alertBody = message
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
}