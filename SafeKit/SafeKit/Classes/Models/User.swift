//
//  User.swift
//  SafeKit
//
//  Created by Sergei Kazberovich on 6/18/16.
//  Copyright © 2016 Elvira. All rights reserved.
//

import UIKit
import Parse

class User: PFUser {
    
    override static func currentUser() -> User? {
        return PFUser.currentUser() as? User
    }
    
    @NSManaged var sex : Int
    @NSManaged var age : Int
    @NSManaged var type : Int
}
